<?php

namespace App\Entity;

use App\Repository\EventRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass=EventRepository::class)
 * @ORM\Table(name="events")
 */
class Event
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\NotBlank
     */
    private $name;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\NotBlank
     */
    private $location;

    /**
     * @ORM\Column(type="decimal", precision=5, scale=2, nullable=true)
     * @Assert\PositiveOrZero
     */
    private $price;

    /**
     * @Assert\NotBlank
     * @Assert\Length(min=15, minMessage="La description de l'évènement doit être au moins de {{ limit }} caractères !")
     * @ORM\Column(type="text")
     */
    private $description;

    /**
     * @Assert\NotBlank
     * @Assert\GreaterThanOrEqual("+1 hour Europe/Paris")
     * @ORM\Column(type="datetime_immutable")
     */
    private $startsAt;

    /**
     * @ORM\Column(type="string", length=255, options={"default":"placeholder.jpg"})
     * @Assert\Regex("/^\w+\.(jpe?g|png)$/i")
     */
    private $imageFileName;

    /**
     * @ORM\Column(type="integer", options={"default":1})
     * @Assert\Positive
     */
    private $capacity;

    /**
     * @ORM\OneToMany(targetEntity=Registration::class, mappedBy="event", orphanRemoval=true)
     */
    private $registrations;

    public function __construct()
    {
        $this->registrations = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(?string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getLocation(): ?string
    {
        return $this->location;
    }

    public function setLocation(?string $location): self
    {
        $this->location = $location;

        return $this;
    }

    public function getPrice(): ?string
    {
        return $this->price;
    }

    public function setPrice(?string $price): self
    {
        $this->price = $price;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(?string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getStartsAt(): ?\DateTimeImmutable
    {
        return $this->startsAt;
    }

    public function setStartsAt(\DateTimeImmutable $startsAt): self
    {
        $this->startsAt = $startsAt;

        return $this;
    }


    public function getCapacity(): ?int
    {
        return $this->capacity;
    }

    public function setCapacity(?int $capacity): self
    {
        $this->capacity = $capacity;

        return $this;
    }

    public function getImageFileName(): ?string
    {
        return $this->imageFileName;
    }

    public function setImageFileName(?string $imageFileName): self
    {
        $this->imageFileName = $imageFileName;

        return $this;
    }

    /**
     * @return Collection|Registration[]
     */
    public function getRegistrations(): Collection
    {
        return $this->registrations;
    }

    public function addRegistration(Registration $registration): self
    {
        if (!$this->registrations->contains($registration)) {
            $this->registrations[] = $registration;
            $registration->setEvent($this);
        }

        return $this;
    }

    public function removeRegistration(Registration $registration): self
    {
        if ($this->registrations->removeElement($registration)) {
            // set the owning side to null (unless already changed)
            if ($registration->getEvent() === $this) {
                $registration->setEvent(null);
            }
        }

        return $this;
    }

    /**
     * Vérifiez si l'événement est gratuit ou non.
     */
    public function isFree(): bool
    {
        return $this->getPrice() == 0 or is_null($this->getPrice());
    }

    /**
     * Le nombre de places restantes pour cet événement.
     */
    public function getSpotsLeft(): int
    {
        return $this->getCapacity() - $this->getRegistrations()->count();
    }

    /**
     * Vérifiez s'il n'y a plus de places disponibles pour cet événement.
     */
    public function isSoldOut(): bool
    {
        return $this->getSpotsLeft() === 0;
    }
}
