<?php

namespace App\Entity;

use App\Repository\RegistrationRepository;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass=RegistrationRepository::class)
 * @ORM\Table(name="registrations")
 */
class Registration
{
    public const HOW_HEARD_OPTIONS = [
        'Facebook' => 'Facebook',
        'Twitter' => 'Twitter',
        'Blog Post' => 'Blog Post',
        'Newsletter' => 'Newsletter',
        'Moteur de recherche' => 'Moteur de recherche',
        'Ami/Collegue' => 'Ami/Collegue',
        'Autre' => 'Autre'
    ];

    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\NotBlank
     */
    private $name;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\NotBlank
     * @Assert\Email
     */
    private $email;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\NotBlank(message="Veuillez choisir une option.")
     * @Assert\Choice(callback="getHowHeardOptions", message="Choisissez une option.")
     */
    private $howHeard;

    /**
     * @ORM\ManyToOne(targetEntity=event::class, inversedBy="registrations")
     * @ORM\JoinColumn(nullable=false)
     */
    private $event;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(string $email): self
    {
        $this->email = $email;

        return $this;
    }

    public function getHowHeard(): ?string
    {
        return $this->howHeard;
    }

    public function setHowHeard(string $howHeard): self
    {
        $this->howHeard = $howHeard;

        return $this;
    }

    public function getEvent(): ?event
    {
        return $this->event;
    }

    public function setEvent(?event $event): self
    {
        $this->event = $event;

        return $this;
    }

    public function getHowHeardOptions(): array
    {
        return array_values(self::HOW_HEARD_OPTIONS);
    }
}
