<?php

namespace App\Controller;

use App\Entity\Event;
use App\Entity\Registration;
use App\Form\RegistrationFormType;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;

class EventRegistrationsController extends AbstractController
{
    #[Route('/events/{event<[0-9]+>}/registrations', methods: ['GET'], name: 'app_event_registrations_index')]
    public function index(Event $event): Response
    {
        $registrations = $event->getRegistrations();

        return $this->render('events/registrations/index.html.twig', compact('event', 'registrations'));
    }

    #[Route('/events/{event<[0-9]+>}/registrations/create', methods: ['GET', 'POST'], name: 'app_event_registrations_create')]
    public function create(Event $event, Request $request, EntityManagerInterface $em): Response
    {
        if ($event->isSoldOut()) {
            throw new AccessDeniedHttpException(sprintf("Désolé, l'événement #%d est complet.", $event->getId()));
        }

        $registration = new Registration;

        $form = $this->createForm(RegistrationFormType::class, $registration);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $registration->setEvent($event);

            $em->persist($registration);
            $em->flush();

            $this->addFlash('success', "Merci, vous êtes inscrit !");

            return $this->redirectToRoute('app_event_registrations_index', ['event' => $event->getId()]);
        }

        return $this->renderForm('events/registrations/create.html.twig', compact('event', 'form'));
    }
}
