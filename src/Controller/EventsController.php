<?php

namespace App\Controller;


use App\Entity\Event;
use App\Form\EventFormType;
use App\Repository\EventRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class EventsController extends AbstractController
{
    #[Route('/', methods: 'GET', name: "app_home")]
    #[Route('/events', methods: 'GET', name: 'app_events_index')]
    public function index(EventRepository $eventRepository): Response
    {
        // $events = $eventRepository->findBy([],['startsAt' => 'ASC', 'price' => 'ASC']);

        $events = $eventRepository->findUpComming();

        return $this->render('events/index.html.twig', compact('events'));
    }

    #[Route('/events/create', methods: ['GET', 'POST'], name: 'app_events_create')]
    public function create(Request $request, EntityManagerInterface $em): Response
    {
        $event = new Event;

        $form = $this->createForm(EventFormType::class, $event);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em->persist($event);
            $em->flush();

            $this->addFlash('success', 'Evenement créé avec succès');

            return $this->redirectToRoute('app_events_show', ['id' => $event->getId()]);
        }

        return $this->renderForm('events/create.html.twig', compact('form'));
    }

    #[Route('/events/{id<[0-9]+>}', methods: 'GET', name: 'app_events_show')]
    public function show(Event $event): Response
    {
        return $this->render('events/show.html.twig', compact('event'));
    }

    #[Route('/events/{id<[0-9]+>}/edit', methods: ['GET', 'POST'], name: 'app_events_edit')]
    public function edit(Event $event, Request $request, EntityManagerInterface $em): Response
    {
        $form = $this->createForm(EventFormType::class, $event);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em->flush();

            $this->addFlash('success', 'Evénement mis à jour.');

            return $this->redirectToRoute('app_events_show', ['id' => $event->getId()]);
        }

        return $this->renderForm('events/edit.html.twig', [
            'event' => $event,
            'form' => $form
        ]);
    }

    #[Route('/events/{id<[0-9]+>}/delete', methods: 'POST', name: 'app_events_delete')]
    public function delete(Request $request, Event $event, EntityManagerInterface $em): Response
    {
        if ($this->isCsrfTokenValid('event_deletion_' . $event->getId(), $request->request->get('csrf_token'))) {
            $em->remove($event);
            $em->flush();

            $this->addFlash(
                'danger',
                sprintf('Evenement %s éffacé !', $event->getName())
            );
        }

        return $this->redirectToRoute('app_events_index');
    }
}
