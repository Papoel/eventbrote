<?php

namespace App\Twig;

use Twig\TwigFilter;
use App\Entity\Event;
use Twig\TwigFunction;
use Twig\Extra\Intl\IntlExtension;
use Twig\Extension\AbstractExtension;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;

class AppExtension extends AbstractExtension
{
    public function __construct(private UrlGeneratorInterface $urlGenerator)
    {
    }

    public function getFilters(): array
    {
        return [new TwigFilter('datetime', [$this, 'formatDateTime']),];
    }

    public function getFunctions(): array
    {
        return [
            new TwigFunction('pluralize', [$this, 'pluralize']),
            new TwigFunction('format_price', [$this, 'formatPrice'], ['is_safe' => ['html']]),
            new TwigFunction('register_link_or_sold_out', [$this, 'registerLinkOrSoldOut'], ['is_safe' => ['html']]),
        ];
    }

    public function pluralize(int $count, string $singular, ?string $plural = null): string
    {
        $plural ??= $singular . 's';

        $string = $count === 1 ? $singular : $plural;

        return "$count $string";
    }

    public function formatPrice(Event $event): string
    {
        return $event->isFree()
            ? '<b>Free!</b>'
            : (new IntlExtension)->formatCurrency($event->getPrice(), 'EUR');
    }

    public function formatDateTime(\DateTimeInterface $dateTime): string
    {
        date_default_timezone_set('Europe/Paris');
        setlocale(LC_ALL, 'fra_fra');
        // return $dateTime->format('F d, Y \a\t h:i A');

        $heure = $dateTime->format('H:i');
        $dateTime = utf8_encode(strftime("%A %d %B %Y"));
        return $dateTime . ' à ' . $heure;
    }

    public function registerLinkOrSoldOut(Event $event): string
    {
        return $event->isSoldOut()
            ? '<span class="p-2 badge bg-danger text-uppercase">🔒 Plus de place</span>'
            : sprintf(
                '<a href="%s" class="btn btn-primary text-uppercase">🗓&nbsp;&nbsp;Inscrivez-vous à cet événement !</a>',
                $this->urlGenerator->generate('app_event_registrations_create', ['event' => $event->getId()])
            );
    }
}
