<?php

namespace App\Form;

use App\Entity\Event;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;

class EventFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', TextType::class, [
                'attr' => [
                    'autofocus' => true
                ],
                'label' => 'Nom'
            ])
            ->add('location', TextType::class,[
                'label' => 'Localisation'
            ])
            ->add('price', NumberType::class, [
                'html5' => true,
                'attr' => ['step' => 0.01],
                'scale' => 2,
                'required' => false,
                'label' => 'Prix'
            ])
            ->add('description', TextareaType::class, [
                'attr' => ['rows' => 5],
                'label' => 'Description'
            ])
            ->add('startsAt', null,[
                'label' => "Date de l'Evénement"
            ])
            ->add('imageFileName', null, [
                'label' => 'Nom du fichier image',
                'empty_data' => 'placeholder.jpg'

            ])
            ->add('capacity', null, [
                'empty_data' => '1',
                'label' => 'Capacités'
            ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Event::class,
        ]);
    }
}
